function add(route) {
  var xhr = new XMLHttpRequest();
  xhr.open("POST", route);
  xhr.send(document.getElementById("pastecontent").value);
  xhr.onload = function() {
    if (xhr.status != 200) {
      document.getElementById("progress").innerText = "HTTP Server error: " + xhr.statusText + ". Reason: " + xhr.responseText;
    } else {
      document.location.href = '/' + this.responseText;
    }
  };
  xhr.onprogress = function() {  document.getElementById("progress").innerText = "Uploading..."; };
  xhr.onerror = function() {  document.getElementById("progress").innerText = "Failed to connect to backend server, please refresh page and try again"; };
}