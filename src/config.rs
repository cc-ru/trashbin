use std::io::{self, BufRead};
use std::path::Path;
use std::{fs::File, io::Write, net::IpAddr, str::FromStr};

#[derive(Clone, Copy)]
pub struct Config {
    pub ip: IpAddr,
    pub port: u16,
    pub lifetime: u64,
    pub max_size: u32,
    pub max_pastes: usize,
}

impl Config {
    pub fn new() -> Self {
        Self {
            ip: IpAddr::from_str("127.0.0.1").unwrap(),
            port: 8000,
            lifetime: 60,
            max_size: 500000000,
            max_pastes: 500,
        }
    }

    pub fn save(&self) -> std::io::Result<()> {
        let mut file = File::create("settings.cfg")?;
        file.write(include_str!("settings_default.cfg").as_bytes())?;
        Ok(())
    }

    pub fn load(&mut self) -> std::io::Result<()> {
        if let Ok(lines) = read_lines("settings.cfg") {
            for line in lines {
                if let Ok(ip) = line {
                    if !ip.starts_with("#") && ip.len() > 1 {
                        let setting_value = ip.splitn(2, "=").collect::<Vec<&str>>();
                        match setting_value[0] {
                        "ip" => { self.ip = IpAddr::from_str(setting_value[1]).expect("Incorrect Ip address format in config. Must be valid ipV4 or ipV6 address") }
                        "port" => { self.port = setting_value[1].parse::<u16>().expect("Incorrect port format in config. Must be integer in valid range (0-65535)") }
                        "lifetime" => { self.lifetime = setting_value[1].parse::<u64>().expect("Incorrect lifetime format in config. Must be positive integer") }
                        "max_size" => { self.max_size = setting_value[1].parse::<u32>().expect("Incorrect size format in config. Must be integer in valid range (0-4294967295)") }
                        "max_pastes" => { self.max_pastes = setting_value[1].parse::<usize>().expect("Incorrect paste format in config. Must be positive integer") }
                        _ => println!("warning: unknown option `{}` it will be ignored", setting_value[0])
                    }
                    }
                }
            }
        } else {
            self.save()?;
        }
        println!("configuration loaded!");
        Ok(())
    }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
