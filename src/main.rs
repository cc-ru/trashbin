mod config;
mod paste;

use bytes::Bytes;
use std::{
    env,
    path::Path,
    sync::{Arc, Mutex},
    time::Duration, collections::HashMap,
};

use warp::{hyper::StatusCode, reply, Filter};

use crate::paste::{Paste, generate_id};

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("{}", include_str!("asciiart.txt"));
    let pastes = Arc::new(Mutex::new(HashMap::new()));
    let mut config = config::Config::new();
    config.load()?;

    let timer = timer::Timer::new();
    let pastes_timer = pastes.clone();

    let _guard = {
        timer.schedule_repeating(chrono::Duration::minutes(1), move || {
            let mut data = pastes_timer.lock().unwrap();
            // remove paste if lifetime exceed 60 minutes
            data.retain(|_, v: &mut Paste| {
                v.lifetime.elapsed() < Duration::from_secs(config.lifetime * 60)
            });

            if data.len() > config.max_pastes {
                data.clear();
                println!("[warning] paste limit reached!");
            }
        })
    };
    println!(
        "version {} launched from: {}:{}",
        VERSION, config.ip, config.port
    );

    {
        let psts = pastes.clone();

        let add_plain_route = warp::post()
            .and(warp::path("addplain"))
            .and(warp::body::content_length_limit(config.max_size as u64))
            .and(warp::body::bytes())
            .map(move |body: Bytes| {
                if !body.is_empty() {
                    let array_len = psts.lock().unwrap().len();
                    let id = generate_id(array_len as i32);
                    psts.lock().unwrap().insert(id, Paste::new_plain(body.to_vec()));
                    reply::with_status(
                        reply::with_header(
                            id.to_string(),
                            "content-type",
                            "text/plain",
                        ),
                        StatusCode::OK,
                    )
                } else {
                    reply::with_status(
                        reply::with_header(
                            "Empty request".to_string(),
                            "content-type",
                            "text/plain",
                        ),
                        StatusCode::BAD_REQUEST,
                    )
                }
            });

        let psts = pastes.clone();

        let add_html_route = warp::post()
            .and(warp::path("add"))
            .and(warp::body::content_length_limit(config.max_size as u64))
            .and(warp::body::bytes())
            .map(move |body: Bytes| {
                if !body.is_empty() {
                    let array_len = psts.lock().unwrap().len();
                    let id = generate_id(array_len as i32);
                    psts.lock().unwrap().insert(id, Paste::new_html(body.to_vec()));
                    reply::with_status(
                        reply::with_header(
                            id.to_string(),
                            "content-type",
                            "text/plain",
                        ),
                        StatusCode::OK,
                    )
                } else {
                    reply::with_status(
                        reply::with_header(
                            "Empty request".to_string(),
                            "content-type",
                            "text/plain",
                        ),
                        StatusCode::BAD_REQUEST,
                    )
                }
            });

        let psts = pastes.clone();

        let get_pastes_route = warp::path!(usize).map(move |id: usize| {
            let pst = psts.lock().unwrap();

            match pst.get(&(id as i32)) {
                Some(paste) => {
                    reply::with_header(
                        String::from_utf8_lossy(&paste.content).into_owned(),
                        "content-type",
                        paste.content_type.clone(),
                    )
                },
                None => {
                    return reply::with_header(
                        format!("paste with id {} not found", id),
                        "content-type",
                        "text/plain",
                    )
                }
            }
        });

        let psts = pastes.clone();

        let get_pastes_total_route = warp::path("total").map(move || {
            let pst = psts.lock().unwrap();
            reply::with_header(format!("{}", pst.len()), "content-type", "text/plain")
        });

        let path = env::current_dir()?.join(Path::new("static"));

        if path.exists() {
            let routes = warp::fs::dir(path)
                .or(add_plain_route)
                .or(add_html_route)
                .or(get_pastes_route)
                .or(get_pastes_total_route);

            warp::serve(routes).run((config.ip, config.port)).await;
        } else {
            eprintln!("error: cannot find static/ folder, throw static/ folder alongside with executable!");
        }
    }

    Ok(())
}
