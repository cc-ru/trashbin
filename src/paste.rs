use std::time::{Instant, SystemTime};

#[derive(Clone)]
pub struct Paste {
    /// Paste lifetime
    pub lifetime: Instant,
    /// HTTP content type
    pub content_type: String,
    /// Paste content
    pub content: Vec<u8>,
}

impl Paste {
    pub fn new_plain(content: Vec<u8>) -> Self {
        Self {
            lifetime: Instant::now(),
            content,
            content_type: "text/plain; charset=utf-8".to_string(),
        }
    }

    pub fn new_html(content: Vec<u8>) -> Self {
        Self {
            lifetime: Instant::now(),
            content,
            content_type: "text/html; charset=utf-8".to_string(),
        }
    }
}

/// Pseudo random id generation
pub fn generate_id(id: i32) -> i32 {
    let secs = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs() as i32;
    let val = id + (secs / 1000);
	let mut l1 = (val >> 16) & 0xffff;
	let mut r1 = val & 0xffff;
	let mut l2;
    let mut r2;
    
    let rnd_seq: f32 = ((((1366.0 * r1 as f32 + 150889.0) % 714025.0) / 714025.0) * 32767.0).round(); 
    
	for _ in 0..3 {
		l2 = r1;
		r2 = l1 ^ rnd_seq as i32;
		l1 = l2;
		r1 = r2;
	}

    let result = (r1 << 16) + l1;

	result
}