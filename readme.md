![Logo](https://i.imgur.com/6OQKadr.png)

[![GitLab pipeline](https://img.shields.io/gitlab/pipeline/cc-ru/trashbin)](https://gitlab.com/cc-ru/trashbin/builds)
[![Crates.io](https://img.shields.io/crates/d/trashbin)](https://crates.io/crates/trashbin)
[![Crates.io](https://img.shields.io/crates/v/trashbin)](https://crates.io/crates/trashbin)

# Trashbin
That's a ULTRA simple pastebin engine written in Rust!
# API-Reference
API is **VERY** simple and does not require any API keys, authorization, etc. Also, it's backward compatible with older trashbin versions

## Specifications:
Uploaded pastes stores directly in memory.

**Default limits (configurable in settings.cfg)**

* paste lifetime: 60 minutes
* max paste size: 5 MB
* max pastes: 500

### Adding pastes:
Add paste with text/html content-type
```
POST /add
BODY: <Content>

Response variants: pasteid, or any error type
```

Add paste with text/plain content-type
```
POST /addplain
BODY: <Content>

Response variants: pasteid, or any error type
```

You can also upload pastes through web interface

### Viewing pastes:
```
GET /[pasteid]

Response: Paste content with text/html content type or 404 Error if paste not found
```


Added in 0.3.0

```
GET /total

Response: Returns total paste count in trashbin instance
```

### Errors:
Any error return 4XX HTTP Code and reason in body with ``text/plain`` content-type:

``The request payload is too large`` (413) - Occurs if request has exceeded upload limit (5 MB by default)

``Empty request`` (400) - Occurs if request body is empty

### Installing

You can install the latest version via cargo:

```cargo install trashbin```

Or install the latest CI build (https://gitlab.com/cc-ru/trashbin/-/pipelines)


### Building and running

Download and install Rust, clone this repository and run:

```cargo build --release```

Go to ``target/release`` folder and run:

```./trashbin```


### Why?
I want to create ULTRA simple pastebin for IRC-bots multiline output - in this use case long-term storage is not needed...
